<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            !Yii::$app->user->can('admin') ? '' : (
                ['label' => 'Patterns', 'items' => [
                    ['label' => 'Основные шаблоны', 'items' => [
                        ['label' => '1. Контейнер свойств', 'url' => ['/patterns/property_container']],
                        ['label' => '2. Делегирование', 'url' => ['/patterns/delegation']],
                        ['label' => '3. Канал событий', 'url' => ['/patterns/event_channel']],
                    ]],
                    ['label' => 'Порождающие шаблоны', 'items' => [
                        ['label' => '4. Абстрактная фабрика', 'url' => ['/patterns/abstract_factory']],
                        ['label' => '5. Фабричный метод', 'url' => ['/patterns/factory_method']],
                        ['label' => '6. Статическая фабрика', 'url' => ['/patterns/static_factory']],
                        ['label' => '7. Простая фабрика', 'url' => ['/patterns/simple_factory']],
                        ['label' => '8. Одиночка', 'url' => ['/patterns/singleton']],
                    ]],
                ]]
            ),
//            !Yii::$app->user->can('admin') ? '' : (
//              ['label' => 'adminPanel', 'items' => [
//                  ['label' => 'index', 'url' => ['admin-panel/index']],
//              ]]
//            ),
            ['label' => 'About', 'url' => ['/site/about']],



            !Yii::$app->user->can('admin') ? '' : (
            ['label' => 'test', 'items' => [
                ['label' => 'test', 'url' => ['/site/test']],
                ['label' => 'vue', 'url' => ['/site/vue']],
            ]]
            ),
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->login . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
