эта документация написана для тех кто только начал тернистый путь в освоении yii2 и vue,
для тех кто хотел бы разобраться как объеденить эти 2 проекта.

документация не учит программированию, а лишь служит для объяснения хода подготовки проекта  

Установка и запуск приложения
-------------------

Для работы нам потребуется установленные: [git](https://git-scm.com/), [composer](https://getcomposer.org/), [npm](https://nodejs.org), [docker](https://www.docker.com/) и [docker-compose](https://docs.docker.com/compose/install/) (предположим что вы уже их установили)

Первым делом нам потребуется где-то хранить все наши файлы, чтобы в любой момент мы могли просто скачать их и продолжить работу или начать новый проект.

Для этого будем использовать ``` git ```

Я выбрал [gitlab.com](https://gitlab.com/). создадим новый проект и назову его  [yii2vue](https://gitlab.com/IssID/yii2vue.git). Теперь пустой проект клонирую к себе.

~~~
git clone https://gitlab.com/IssID/yii2vue.git
~~~

затем рядом скачиваем базовый проект yii2 в папку basic

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic basic
~~~

Теперь нужно переместить все из папки basic в папку ```yii2vue``` , это можно сделать вручную. У вас должно получиться примерно так

~~~
 | yii2vue
 |--> .git
 |--> assets
 |--> commands
 |--> config
 |--> controllers
     ...
~~~
папка ```.git``` может быть скрыта

Сохраним наш проект в ```git``` в главную ветку ```master```

Проверяем наличие новых файлов ```git status``` появиться список файлов (обычно подкрашен красным. это означает что файл не добавлен в git)

Добавляем все файлы к сохранению ```git add``` теперь можно еще раз проверить наши файлы ```git status``` (теперь файлы должны быть зелеными и подписаны как новый файл:). 
Создаем  ```git commit -m 'first commit'``` (git оповести нас "create mode"). Загружаем в наше хранилище свежий проект  ```git push -u origin master```

Проверяем наш репозиторий на [gitlab.com](https://gitlab.com/IssID/yii2vue.git) в нем должны появиться файлы.

Для запуска сервера воспользуемся командой 
~~~
docker-compose up -d --build
~~~

открываем в браузере [http://localhost:8000/](http://localhost:8000/) и проверяем работу нашего сайта

 ~~~
                    Congratulations!
 You have successfully created your Yii-powered application.
 ~~~

Настройка приложения
-------------------

Попробуем открыть страницу contact, и видим что адрес страницы изменился на

[http://localhost:8000/index.php?r=site%2Fcontact](http://localhost:8000/index.php?r=site%2Fcontact)

Такой вид не очень удобен для восприятия человеком. Давайте поправим.

открываем файл  ```config/web.php```

в **components** добавляем
~~~
'urlManager' => [
   'enablePrettyUrl' => true,
   'enableStrictParsing' => false,
   'showScriptName' => false,
],
~~~
а в **request** добавляем
~~~
'parsers' => [
   'application/json' => 'yii\web\JsonParser',
],
~~~

должно получиться
~~~
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
~~~
 
попробуем обновить нашу страницу. 
Теперь у нас ссылка выглядит так [http://localhost:8000/site/contact](http://localhost:8000/site/contact) это более удобно для восприятия человека. 
Если не работает, проверьте наличие файла ```.htaccess``` в папке ```web```, если его нет, создайте и добавьте в него эти строчки

~~~
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule . index.php [L]
~~~

Установим dotenv
-------------------

Этот файл будет в себе содержать приватную конфигурацию нашего проекта. для этого воспользуемся командой

~~~
composer require josegonzalez/dotenv
~~~

создадим файл ```.env.dist``` (общий файл) и файл ```.env``` (приватный файл) добавим запись в файл ```.gitignore``` название нашего файла ```.env``` чтобы его не добавлять в наш репозиторий.

Создадим в .env тестовую переменную TEST=value (в .env.dist нужно дублировать наши переменные но с тестовыми значениями) давайте подключим к нашему проекту  ```dotenv```

Откроем файл ```web/index.php``` и после строчке

~~~
require __DIR__. '/../vendor/autoload.php';
~~~

добавим строчку
~~~
(new josegonzalez\Dotenv\Loader(__DIR__ . '/../.env'))->parse()->putenv(true)->define();
~~~

и сразу для проверки добавим

~~~
echo getenv( 'TEST' ); exit ;
~~~

должно выглядеть так
~~~
<?php
require __DIR__ . '/../vendor/autoload.php';
(new josegonzalez\Dotenv\Loader(__DIR__ . '/../.env'))->parse()->putenv(true)->define();
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
~~~

обновляем страницу и видим что у нас на странице появилось слово value.  Все работает, строчку ```echo getenv( 'TEST' ); exit;``` можно удалять.

Точно так же добавьте строчку
~~~
(new josegonzalez\Dotenv\Loader(__DIR__ . '/.env'))->parse()->putenv(true)->define();
~~~

в файл ./yii.php будьте внимательны(путь к файлу .env отличается)

в файлах ```./yii.php``` и ```./web/index.php``` удалите строчки

~~~
// comment out the following two lines when deployed to production

defined( 'YII_DEBUG' ) or define( 'YII_DEBUG' , true );

defined( 'YII_ENV' ) or define( 'YII_ENV' , 'dev' );
~~~


Установим базу данных
-------------------

Нам потребуется где-то хранить данные. Воспользуемся одной из баз под названием ```mariadb``` для этого развернем свой сервер так же как и сервер с приложением

изменим наш файл ```docker-compose.yml``` следующим образом

~~~
version: '2'
services:
  php:
    image: yiisoftware/yii2-php:7.2-apache
    container_name: yii2vue
    volumes:
      - ~/.composer-docker/cache:/root/.composer/cache:delegated
      - .:/app:delegated
    ports:
      - '8000:80'
    links:
      - 'mariadb'

  mariadb:
    image: mariadb:10.1
    container_name: mariadb
    command: mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci
    ports:
      - 3306:3306
    volumes:
      - mariadb:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: "Wohch0Uv"
      MYSQL_USER: 'yii2vue'
      MYSQL_PASSWORD: 'Een3Ugh3'
      MYSQL_DATABASE: 'yii2vue'

volumes:
  mariadb:

~~~

в файлах ```.env``` и ```.env.dist``` добавим
~~~
# yii
YII_DEBUG=true
YII_ENV=dev

# db
DB_DSN=mysql:host=mariadb;dbname=yii2vue
DB_USER=yii2vue
DB_PWD=Een3Ugh3
~~~

и отредактируем файл ```.config/db.php```

~~~
return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv('DB_DSN'),
    'username' => getenv('DB_USER'),
    'password' => getenv('DB_PWD'),
    'charset' => 'utf8',
~~~

Пересобираем наш докер проект 
~~~
docker-compose up -d --build
~~~

Добавляем миграции
-------------------
Теперь нам потребуется подготовить все для авторизации пользователей и уровня доступа для них. Для этого воспользуемся миграцией.

добавим в настройки в **&#39;components&#39;** следующие строчки

~~~
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache'
        ],
~~~

Зайдем в наш контейнер с севером базы данных

командой 
~~~
docker ps
~~~
узнаем id контейнера в моем случае это ```b5da85d11540``` у вас может быть другой. Заходим в контейнер 
~~~
docker exec -it b5da85d11540 bash
~~~
Выполняем команду для миграции доступов пользователям

~~~
./yii migrate --migrationPath=@yii/rbac/migrations
~~~

Создаем комманду для загрузки уровней доступа пользователей. Создаем файл ```app\commands\RbacController.php```
~~~
<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // добавляем разрешение "createPost"
        $createPost = $auth->createPermission('CREATE_POST_PERMISSION');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // добавляем разрешение "updatePost"
        $updatePost = $auth->createPermission('UPDATE_POST_PERMISSION');
        $updatePost->description = 'Update post';
        $auth->add($updatePost);

        // добавляем роль "author" и даём роли разрешение "createPost"
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPost);

        // просто пользователь без доступов
        $user = $auth->createRole('user');
        $auth->add($user);

        // добавляем роль "admin" и даём роли разрешение "updatePost"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $user);

        //устанавливаем первому пользователю права администратора
        $auth->assign($admin, 1);
    }
}
~~~
Выполняем комманду из контейнера ```./yii rbac/init```

Создаем новую таблицу для хранения пользователей

~~~
./yii migrate/create create_users_table
~~~

Редактируем новый файл в папке ```./mogrations/m190926_115829_create_users_table.php```
~~~
<?php

use yii\db\Migration;

/**
* m190926_115829_create_user_table
*/
class m190926_115829_create_user_table extends Migration
{
   /**
    * {@inheritdoc}
    * @throws \yii\base\Exception
    */
   public function safeUp()
   {
       $tableOptions = null;

       if ($this->db->driverName === 'mysql') {
           $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
       }

       $this->createTable('user', [
           'id' => $this->primaryKey(),
           'login' => $this->char(64)->notNull()->unique(),
           'password' => $this->char(255)->notNull(),
           'password_reset_token' => $this->char(255)->unique(),
           'access_token' => $this->char(64),
           'auth_key' => $this->char(64),
           'email' => $this->string()->notNull()->unique(),
           'status' => $this->smallInteger()->defaultValue(10),
           'created_at' => $this->integer(),
           'updated_at' => $this->integer(),
       ], $tableOptions);

       $this->createIndex('usersAccessToken', 'user', ['access_token'], true);
       $this->createIndex('usersLogin', 'user', ['login'], true);
       $this->createIndex('usersEmail', 'user', ['email'], true);

       $this->insert('user', [
           'id' => 1,
           'login' => 'root',
           'password' => '$2y$13$wZfTFKQLANy.OnqUhv0L/ubxwmDF/Xb3a3xCuqZTpkApA8eH3YY7G',
           'password_reset_token' => '',
           'access_token' => \Yii::$app->security->generateRandomString(),
           'auth_key' => null,
           'email' => 'example@example.com',
           'status' => null, // для активации пользователя установить 10
           'created_at' => null,
           'updated_at' => null,
       ]);
   }

   /**
    * {@inheritdoc}
    */
   public function safeDown()
   {
       $this->dropTable('user');
   }
}
~~~

обратите внимание что название класса должно совпадать с названием файла (в вашем случае файл может называться по другому)

выполняем миграцию 
~~~
./yii migrate/up
~~~

Логика авторизации
-------------------

Отредактируем существующую модель ```user.php```
~~~
<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
* This is the model class for table "user".
*
* @property int $id
* @property string $login
* @property string $password
* @property string $password_reset_token
* @property string $access_token
* @property string $auth_key
* @property string $email
* @property int $status
* @property int $created_at
* @property int $updated_at
*/
class User extends ActiveRecord implements IdentityInterface
{

   const STATUS_DELETED = 0;
   const STATUS_ACTIVE = 10;

   /**
    * @inheritdoc
    */
   public static function tableName()
   {
       return '{{%user}}';
   }

   /**
    * @inheritdoc
    */
   public function behaviors()
   {
       return [
           TimestampBehavior::className(),
       ];
   }

   /**
    * @inheritdoc
    */
   public function rules()
   {
       return [
           ['status', 'default', 'value' => self::STATUS_ACTIVE],
           ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
       ];
   }

   /**
    * @param int|string $id
    * @return User|IdentityInterface|null
    */
   public static function findIdentity($id)
   {
       return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
   }

   /**
    * @param mixed $token
    * @param null $type
    * @return void|IdentityInterface|null
    * @throws NotSupportedException
    */
   public static function findIdentityByAccessToken($token, $type = null)
   {
       throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
   }

   /**
    * Finds user by login
    *
    * @param string $login
    * @return static|null
    */
   public static function findByLogin($login)
   {
       return static::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
   }

   /**
    * @return int|mixed|string
    */
   public function getId()
   {
       return $this->getPrimaryKey();
   }

   /**
    * @return string
    */
   public function getAuthKey()
   {
       return $this->auth_key;
   }

   /**
    * @param string $authKey
    * @return bool
    */
   public function validateAuthKey($authKey)
   {
       return $this->getAuthKey() === $authKey;
   }

   /**
    * Validates password
    *
    * @param string $password password to validate
    * @return bool if password provided is valid for current user
    */
   public function validatePassword($password)
   {
       return Yii::$app->security->validatePassword($password, $this->password);
   }

   /**
    * Generates password hash from password and sets it to the model
    *
    * @param $password
    * @throws \yii\base\Exception
    */
   public function setPassword($password)
   {
       $this->password = Yii::$app->security->generatePasswordHash($password);
   }

   /**
    * Generates "remember me" authentication key
    *
    * @throws \yii\base\Exception
    */
   public function generateAuthKey()
   {
       $this->auth_key = Yii::$app->security->generateRandomString();
   }
}

~~~

Создаем форму регистрации ```./models/SignupForm.php```
~~~
<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class SignupForm
 * @package app\models
 */
class SignupForm extends Model
{
    public $login;
    public $password;
    public $email;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmailUnique'],
        ];
    }

    /**
     * Email checker
     * @param $attribute
     * @param $params
     */
    public function validateEmailUnique($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUserByEmail();

            if ($user) {
                $this->addError($attribute, 'Email already exists.');
            }
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUserByEmail()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    /**
     * @return User|null
     * @throws \yii\base\Exception
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->login = $this->login;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save(false);

            // нужно добавить следующие три строки:
            $auth = Yii::$app->authManager;
            $userRole = $auth->getRole('user');
            $auth->assign($userRole, $user->getId());

            return $user;
        }

        return null;
    }
}

~~~

Добавляем регистрацию в контроллер ```./controllers/SiteController.php```
~~~
<?php

namespace app\controllers;

//...
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;

class SiteController extends Controller
{

   //...

   public function actionSignup()
   {
       $model = new SignupForm();

       if ($model->load(Yii::$app->request->post())) {
           if ($user = $model->signup()) {
               if (Yii::$app->getUser()->login($user)) {
                   return $this->goHome();
               }
           }
       }

       return $this->render('signup', [
           'model' => $model,
       ]);
   }

}

~~~

Создаем вьюшку [./views/site/signup.php](https://gitlab.com/IssID/yii2vue.git)
~~~
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
   <h1><?= Html::encode($this->title) ?></h1>
   <p>Please fill out the following fields to signup:</p>
   <div class="row">
       <div class="col-lg-5">

           <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
           <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
           <?= $form->field($model, 'email') ?>
           <?= $form->field($model, 'password')->passwordInput() ?>
           <div class="form-group">
               <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
           </div>
           <?php ActiveForm::end(); ?>

       </div>
   </div>
</div>

~~~

в файле ```./models/LoginForm.php``` меняем ```username``` на ```login```

тоже самое в файле ```./views/site/login.php``` и ```./views/layouts/main.php```

Для входа можете использовать login ```root``` и password ```root```
или создать нового пользователя

Создадим модуль api
-------------------

В файле ```config/web.php``` добавим в начало файла

~~~
$rules = require __DIR__. '/rules.php' ;
$modules = require __DIR__. '/modules.php' ;
~~~

в конце поправим
~~~
   'urlManager' => [
       'enablePrettyUrl' => true,
       'showScriptName' => false,
       'enableStrictParsing' => false,
       'rules' => $rules,
   ],
],
'modules' => $modules,
'params' => $params,
~~~

создадим 2 файла **rules.php**

~~~
<?php

return [
   '/' => 'site/index',
   '/vue' => 'site/vue',

   // api
   'GET /api' => 'api/default/index',
];

~~~

и **modules.php**

~~~
<?php

return [
   'api' => [
       'class' => 'app\modules\api\Module',
   ],
];
~~~

Создадим папку ```./modules/api/``` в ней файл ```Module.php``` и папку ```controllers``` в ней файл ```DefaultController.php``` в него сразу добавим

~~~
<?php

namespace app\modules\api\controllers;

use yii\rest\Controller;

class DefaultController extends Controller
{
   public function actionIndex()
   {
       return ['api' =>'test'];
   }
}
~~~

а в файл [Module.php](https://gitlab.com/IssID/yii2vue.git)

~~~
а в файл Module.php

<?php

namespace app\modules\api;

class Module extends \yii\base\Module
{
   public $controllerNamespace = 'app\modules\api\controllers';
}

~~~

проверим api [http://localhost:8000/api](http://localhost:8000/api)

~~~
<response>
    <api>test</api>
</response>
~~~

обратите внимание чтобы использовать функции ```rest-api``` мы контроллер ```DefaultController``` унаследовали от ```yii\rest\Controller```

обычный контроллер наследуется от ```yii\web\Controller```

Подключаем vue
-------------------

Нам потребуется создать несколько файлов. В корне проекта создадим  [./package.json](https://github.com/laravel/laravel/blob/master/package.json)
~~~
{
    "private": true,
    "scripts": {
        "dev": "npm run development",
        "development": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
        "watch": "npm run development -- --watch",
        "watch-poll": "npm run watch -- --watch-poll",
        "hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
        "prod": "npm run production",
        "production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
    },
    "devDependencies": {
        "axios": "^0.19",
        "cross-env": "^5.1",
        "laravel-mix": "^4.0.7",
        "lodash": "^4.17.13",
        "resolve-url-loader": "^2.3.1",
        "sass": "^1.15.2",
        "sass-loader": "^7.1.0"
    }
}
~~~

файл [./webpack.mix.js](https://github.com/laravel/laravel/blob/master/webpack.mix.js)
~~~
let mix = require('laravel-mix');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for the application as well as bundling up all the JS files.
|
*/

mix.setPublicPath("./web");
mix.js('vue/app.js', 'web/js');
//mix.sass('resources/assets/sass/app.scss', 'web/css');

~~~

создадим ./vue/app.js
~~~
import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './components/App.vue';
import Menu from "./components/Menu";

Vue.component('Menu', Menu);

Vue.use(VueRouter);

const routes = [
   {
       path: '*',
       component: Menu
   }
];

const router = new VueRouter ({
   mode: 'history',
   routes
});

import axios from 'axios';
axios.defaults.headers.common['Authorization'] = 'Bearer 100-token';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.post['Content-type'] = 'application/json; charset=UTF-8';
axios.defaults.responseType = 'json';

new Vue({
   router,
   el: '#app',
   render: h => h(App)
});

~~~

создадим ./vue/components/app.vue

~~~
<template>
   <div>
       <h1>{{title}}</h1>
       <ErrorWidget></ErrorWidget>

       <router-view></router-view>
   </div>
</template>

<script>

   export default {
       data: function () {
           return {
               title: "Title"
           }
       },
       created: function () {
           let vm = this;
           this.$root.$on('update:title', function (title) {
               vm.title = title;
           })
       },
       mounted() {
           this.$root.$emit('update:title', 'App');
       },

   }
</script>
~~~


и ./vue/components/menu.vue
~~~
<template>
   <ul id="menu" class="vue-menu list-group">
       <li v-for="item in items" class="list-group-item">
           <router-link v-bind:to="item.path" class="btn btn-success">{{ item.title }}</router-link>
       </li>
   </ul>
</template>

<script>
   export default {
       name: "menu",
       data() {
           return {
               items: [
                   {
                       path: '/vue',
                       title: 'Vue test'
                   },
               ]
           }
       },
   }
</script>

<style scoped>
   .vue-menu li > * {
       width: 100%;
   }
</style>

~~~

Теперь добавим ```actionVue``` в контроллер ```SiteController``` в который добавим рендер страницы 
~~~
return $this->render('vue');
~~~

создадим саму страницу ```./views/site/vue.php``` с содержанием
~~~
<div id="app"></div>
<?php  $this->registerJsFile('/js/app.js'); ?>
~~~

в .gitignore добавим
~~~
#vue
/web/js/app.js
~~~

также добавляем правила в файл ```rules.php```

~~~
'/vue' => 'site/vue',
~~~

Нам потребуется менеджер пакетов ```npm```. Выполняем команду 
~~~
npm run dev
~~~

ждем когда соберется наш файл ```/web/js/app.js```

[http://localhost:8000/vue](http://localhost:8000/vue)


Обновление пакетов при старте контейнера
-------------------

добавим в файл ```docker-compose.yml```
~~~
composer:
 restart: 'no'
 image: composer/composer:php7
 container_name: composer
 command: install --ignore-platform-reqs
 volumes:
   - .:/app

npm:
 restart: 'no'
 image: node:8
 container_name: npm
 command: npm install --prefix /app
 volumes:
   - .:/app:delegated
~~~

Теперь чтобы скомпилировать vue.js можно запускать команду
~~~
docker-compose run --rm npm  npm run dev --prefix /app
~~~
 и нам на компьюторе не потребуется установленный composer, node, php, и тд. достаточно только docker и docker compose
почему мы не сделали этого в самом начале ?
во-первых нам нужно было разобраться как работают эти программы. 
во-вторых без них мы не смогли бы собрать наш проект
в третьих почему бы и нет.

CORS
-------------------

в `index.php` добавить 
~~~
header('Access-Control-Allow-Origin: *');
~~~

в `behaviors` контроллера добавить 
~~~
$behaviors = parent::behaviors();

...code...

$auth = $behaviors['authenticator'];
unset($behaviors['authenticator']);

$behaviors['corsFilter'] = [
    'class' => \yii\filters\Cors::class,
    'cors' => [
        'Origin' => explode(',', getenv('ALLOWED_ORIGIN')) ?? [],
        'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
        'Access-Control-Allow-Credentials' => true,
        'Access-Control-Allow-Headers' => ['*'],
    ],
];

$behaviors['authenticator'] = $auth;
$behaviors['authenticator']['except'] = ['options'];

return $behaviors;
~~~
