<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AboutAsset
 * @package app\assets
 */
class AboutAsset extends AssetBundle
{
    /** @var string */
    public $sourcePath = '@app/assets/AboutAsset';

    // Путь из web до директории ресурсов
    public $baseUrl = '/AboutAsset';

    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
