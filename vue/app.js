import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './components/App.vue';
import Menu from "./components/Menu";

Vue.component('Menu', Menu);

Vue.use(VueRouter);

const routes = [
    {
        path: '*',
        component: Menu
    }
];

const router = new VueRouter ({
    mode: 'history',
    routes
});

import axios from 'axios';
axios.defaults.headers.common['Authorization'] = 'Bearer 100-token';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.post['Content-type'] = 'application/json; charset=UTF-8';
axios.defaults.responseType = 'json';

new Vue({
    router,
    el: '#app',
    render: h => h(App)
});
