<?php

return [
    'api' => [
        'class' => 'app\modules\api\Module',
    ],

    // https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F

    // Fundamental
    'PropertyContainer' => [
        'class' => 'app\modules\Patterns\PropertyContainer\Module',
    ],

    'Delegation' => [
        'class' => 'app\modules\Patterns\Delegation\Module',
    ],
    'EventChannel' => [
        'class' => 'app\modules\Patterns\EventChannel\Module',
    ],

    // Creating
    'AbstractFactory' => [
        'class' => 'app\modules\Patterns\AbstractFactory\Module',
    ],
    'FactoryMethod' => [
        'class' => 'app\modules\Patterns\FactoryMethod\Module',
    ],
    'StaticFactory' => [
        'class' => 'app\modules\Patterns\StaticFactory\Module',
    ],
    'SimpleFactory' => [
        'class' => 'app\modules\Patterns\SimpleFactory\Module',
    ],

    'Singleton' => [
        'class' => 'app\modules\Patterns\Singleton\Module',
    ],

];
