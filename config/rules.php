<?php

return [
    '/' => 'site/index',

    // ampleadmin
    '/admin-panel/index' => 'AdminPanel/default/index',

    // api
    'GET /api' => 'api/default/index',

    // patterns
    'GET /patterns/property_container' => 'PropertyContainer/default/index',
    'GET /patterns/delegation' => 'Delegation/default/index',
    'GET /patterns/event_channel' => 'EventChannel/default/index',

    'GET /patterns/abstract_factory' => 'AbstractFactory/default/index',
    'GET /patterns/factory_method' => 'FactoryMethod/default/index',
    'GET /patterns/static_factory' => 'StaticFactory/default/index',
    'GET /patterns/simple_factory' => 'SimpleFactory/default/index',

    'GET /patterns/singleton' => 'Singleton/default/index',
];
