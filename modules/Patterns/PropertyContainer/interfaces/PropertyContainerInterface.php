<?php

namespace app\modules\Patterns\PropertyContainer\interfaces;

/**
 * Interface PropertyContainerInterface
 * @package app\modules\Patterns\models\interfaces
 */
interface PropertyContainerInterface
{
    /**
     * @param $propertyName
     * @param $value
     * @return void
     */
    public function addProperty($propertyName, $value);

    /**
     * @param $propertyName
     * @return void
     */
    public function deleteProperty($propertyName);

    /**
     * @param $propertyName
     * @return mixed|null
     */
    public function getProperty($propertyName);

    /**
     * @param $propertyName
     * @param $value
     * @return void
     */
    public function setProperty($propertyName, $value);
}