<?php

namespace app\modules\Patterns\PropertyContainer;

/**
 * Class Module
 * @package app\modules\Patterns\PropertyContainer
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\PropertyContainer\controllers';
}
