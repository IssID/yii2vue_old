<?php

namespace app\modules\Patterns\PropertyContainer\models;

use phpDocumentor\Reflection\Types\Integer;

/**
 * Class BlogPost
 * @package app\modules\Patterns\models
 */
class BlogPost extends PropertyContainer
{
    /** @var string */
    private $title;

    /** @var  integer */
    private $category_id;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param $id
     * @return Integer
     */
    public function getCategory()
    {
        return $this->category_id;
    }

    /**
     * @param $id
     */
    public function setCategory($id)
    {
        $this->category_id = $id;
    }


}