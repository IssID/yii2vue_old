<?php

namespace app\modules\Patterns\PropertyContainer\controllers;

use app\modules\Patterns\PropertyContainer\models\BlogPost;
use app\modules\Patterns\PropertyContainer\models\Description;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\PropertyContainer\controllers
 */
class DefaultController extends Controller
{
    /**
     * Контейнер Свойств
     *
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $title = 'Контейнер Свойств';
        $discription = Description::getDescription();

        // эти данные реализуются должны быть вынесены в форму (это всего лишь пример логики)
        $item = new BlogPost();

        $item->setTitle('Заголовок статьи');
        $item->setCategory(10);

        $item->addProperty('view_count', 100);

        $item->addProperty('last_update', '2020-07-01');
        $item->setProperty('last_update', '2020-07-02');

        $item->addProperty('read_only', true);
        $item->deleteProperty('read_only');

        return $this->render('index', compact('item', 'title', 'discription'));
    }
}
