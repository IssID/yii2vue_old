<?php

namespace app\modules\Patterns\FactoryMethod\models;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;
use app\modules\Patterns\FactoryMethod\interfaces\FormInterface;

abstract class AbstractForm implements FormInterface
{
    /**
     * Рисуем форму
     */
    public function render()
    {
        $guiKit = $this->createGuiKit();
        $result[] = $guiKit->buildButton()->drow();
        $result[] = $guiKit->buildCheckBox()->drow();

        return $result;
    }

    /**
     * Получаем инструментарий для рисования
     *
     * @return GuiFactoryInterface
     */
    abstract function createGuiKit(): GuiFactoryInterface;
}