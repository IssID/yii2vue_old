<?php
namespace app\modules\Patterns\FactoryMethod\models;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;
use app\modules\Patterns\AbstractFactory\models\SemanticUiFactory;

class SemanticUiDielogForm extends AbstractForm
{
    /**
     * @return GuiFactoryInterface
     */
    function createGuiKit(): GuiFactoryInterface
    {
        return new SemanticUiFactory();
    }
}