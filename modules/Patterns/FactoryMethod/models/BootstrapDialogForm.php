<?php
namespace app\modules\Patterns\FactoryMethod\models;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;
use app\modules\Patterns\AbstractFactory\models\BootstrapFactory;

class BootstrapDialogForm extends AbstractForm
{
    /**
     * Получаем инструментарий для рисования
     *
     * @return GuiFactoryInterface
     */
    public function createGuiKit(): GuiFactoryInterface
    {
        return new BootstrapFactory();
    }
}