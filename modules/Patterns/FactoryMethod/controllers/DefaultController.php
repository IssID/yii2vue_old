<?php

namespace app\modules\Patterns\FactoryMethod\controllers;

use app\modules\Patterns\FactoryMethod\models\BootstrapDialogForm;
use app\modules\Patterns\FactoryMethod\models\SemanticUiDielogForm;
use yii\debug\models\search\Debug;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\FactoryMethod\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $item = [
            'title' => 'Фабричный метод',
            'body' => 'FactoryMethod'
        ];

        $dialogForm = new BootstrapDialogForm();
//        $dialogForm = new SemanticUiDielogForm();
        $result = $dialogForm->render();

        return $this->render('index', compact('item', 'result'));
    }
}
