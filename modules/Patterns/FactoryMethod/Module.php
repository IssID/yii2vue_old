<?php

namespace app\modules\Patterns\FactoryMethod;

/**
 * Class Module
 * @package app\modules\Patterns\FactoryMethod
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\FactoryMethod\controllers';
}
