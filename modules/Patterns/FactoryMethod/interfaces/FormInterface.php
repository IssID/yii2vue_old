<?php
namespace app\modules\Patterns\FactoryMethod\interfaces;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;

interface FormInterface
{
    public function render();
    public function createGuiKit(): GuiFactoryInterface;
}