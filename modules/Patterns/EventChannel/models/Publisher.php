<?php

namespace app\modules\Patterns\EventChannel\models;

use app\modules\Patterns\EventChannel\interfaces\EventChannelInterface;
use app\modules\Patterns\EventChannel\interfaces\PublisherInterface;

/**
 * Class Publisher
 * @package app\modules\Patterns\EventChannel\models
 */
class Publisher implements PublisherInterface
{
    /** @var string */
    private $topic;

    /** @var EventChannelInterface */
    private $eventChannel;

    /**
     * Publisher constructor.
     *
     * @param string $topic
     * @param EventChannelInterface $eventChannel
     */
    public function __construct($topic, EventChannelInterface $eventChannel)
    {
        $this->topic = $topic;

        $this->eventChannel = $eventChannel;
    }

    /**
     * Уведомлять подписчиков
     *
     * @param string $data
     * @return mixed|void
     */
    public function publish($data)
    {
        return $this->eventChannel->publish($this->topic, $data);
    }
}