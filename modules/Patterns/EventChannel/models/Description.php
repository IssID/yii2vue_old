<?php

namespace app\modules\Patterns\EventChannel\models;

class Description
{
    /**
     * @return string
     */
    public static function getDescription()
    {
        return "<pre>
        Канал событий (англ. event channel) — фундаментальный шаблон проектирования, 
        используется для создания канала связи и коммуникации через него посредством событий.
        
        Этот канал обеспечивает возможность разным издателям публиковать события и подписчикам, 
        подписываясь на них, получать уведомления.
        
        <a target='_blank' href='https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D0%BD%D0%B0%D0%BB_%D1%81%D0%BE%D0%B1%D1%8B%D1%82%D0%B8%D0%B9_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)'>https://ru.wikipedia.org/wiki/Канал_событий_(шаблон_проектирования)</a>
        </pre>";

    }
}