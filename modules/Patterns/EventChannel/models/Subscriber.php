<?php

namespace app\modules\Patterns\EventChannel\models;

use app\modules\Patterns\EventChannel\interfaces\SubscriberInterface;

/**
 * Class Subscriber
 * @package app\modules\Patterns\EventChannel\models
 */
class Subscriber implements SubscriberInterface
{
    /** @var string */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Уведомить подписчика
     *
     * @param string $data
     * @return mixed
     */
    public function notify($data)
    {
        return "{$this->getName()} оповещен(а) данными [{$data}]";
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}