<?php

namespace app\modules\Patterns\EventChannel\models;

use app\modules\Patterns\EventChannel\interfaces\EventChannelInterface;
use app\modules\Patterns\EventChannel\interfaces\SubscriberInterface;

/**
 * Class EventChannel
 * @package app\modules\Patterns\EventChannel\models
 */
class EventChannel implements EventChannelInterface
{
    /** @var array  */
    private $topics = [];

    /**
     * Издатель уведомляет всех кто подписан на $topic
     *
     * @param string $topic
     * @param $data
     * @return mixed|void
     */
    public function publish($topic, $data)
    {
        $res = [];
        if (empty($this->topics[$topic])) {
            return;
        }

        foreach ($this->topics[$topic] as $subscriber) {
            /** @var SubscriberInterface $subscriber */
            $res[] = $subscriber->notify($data);
        }
        return $res;
    }

    /**
     * Подписчик подписывается на событие $topic
     *
     * @param string $topic
     * @param $subscriber
     * @return mixed
     */
    public function subscribe($topic, SubscriberInterface $subscriber)
    {
        $this->topics[$topic][] = $subscriber;
        return "{$subscriber->getName()} подписан на {$topic}";
    }
}