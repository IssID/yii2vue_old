<?php

namespace app\modules\Patterns\EventChannel\models;

/**
 * Class EventChannelJob
 * @package app\modules\Patterns\EventChannel\models
 */
class EventChannelJob
{
    const FIRST_NEWS = 'first-news';
    const SECOND_NEWS = 'second-news';

    public function run()
    {
        $result = [];

        $newsChannel = new EventChannel();

        $firstGroup = new Publisher(self::FIRST_NEWS, $newsChannel);

        // публишеры одного события но для разных груп
        $secondGroup = new Publisher(self::SECOND_NEWS, $newsChannel);
        $secondNews = new Publisher(self::SECOND_NEWS, $newsChannel);

        //подписчики
        $alexander = new Subscriber('Сашка');
        $alexey = new Subscriber('Лешка');
        $sanPalich = new Subscriber('Сан Палыч');
        $leonid = new Subscriber('Лёня');

        //подписчики подписываются
        $result['subscribe'][] = $newsChannel->subscribe(self::FIRST_NEWS, $alexander);
        $result['subscribe'][] = $newsChannel->subscribe(self::SECOND_NEWS, $alexey);

        $result['subscribe'][] = $newsChannel->subscribe(self::FIRST_NEWS, $sanPalich);
        $result['subscribe'][] = $newsChannel->subscribe(self::SECOND_NEWS, $sanPalich);

        $result['subscribe'][] = $newsChannel->subscribe(self::SECOND_NEWS, $leonid);

        //Оповещение
        $result['publish']['firstGroup'] = $firstGroup->publish('new first group post');
        $result['publish']['secondGroup'] = $secondGroup->publish('new second group post');
        $result['publish']['secondNews'] = $secondNews->publish('this offtop second group post');

        return $result;
    }
}