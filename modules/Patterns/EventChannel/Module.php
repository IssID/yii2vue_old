<?php

namespace app\modules\Patterns\EventChannel;

/**
 * Class Module
 * @package app\modules\Patterns\EventChannel
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\EventChannel\controllers';
}
