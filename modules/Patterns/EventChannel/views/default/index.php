<?php
/**
 * @var $item array
 * @var $title string
 * @var $discription string
 */


use yii\helpers\Html;
use yii\helpers\VarDumper;

$this->title = $title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p><?= $discription ?></p>

<pre>
<?php VarDumper::dump($item); ?>
</pre>
