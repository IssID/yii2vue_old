<?php

namespace app\modules\Patterns\EventChannel\interfaces;

/**
 * Interface SubscriberInterface
 * @package app\modules\Patterns\EventChannel\interfaces
 */
interface SubscriberInterface
{
    /**
     * Уведомить подписчика
     *
     * @param string $data
     * @return mixed
     */
    public function notify($data);

    /**
     * @return mixed
     */
    public function getName();
}