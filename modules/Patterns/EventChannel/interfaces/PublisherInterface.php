<?php

namespace app\modules\Patterns\EventChannel\interfaces;

/**
 * Interface PublisherInterface
 * @package app\modules\Patterns\EventChannel\interfaces
 */
interface PublisherInterface
{
    /**
     * Уведомлять подписчиков
     *
     * @param string $data
     * @return mixed
     */
    public function publish($data);
}