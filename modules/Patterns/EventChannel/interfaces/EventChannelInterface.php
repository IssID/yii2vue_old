<?php

namespace app\modules\Patterns\EventChannel\interfaces;

/**
 * Interface EventChannelInterface
 * @package app\modules\Patterns\EventChannel\interfaces
 *
 * Интерфейс канала событий
 * Связующее звено между подписчиками и издателями
 */
interface EventChannelInterface
{
    /**
     * Издатель уведомляет всех кто подписан на $topic
     *
     * @param string $topic
     * @param $data
     * @return mixed
     */
    public function publish($topic, $data);

    /**
     * Подписчик подписывается на событие $topic
     *
     * @param string $topic
     * @param $subscriber
     * @return mixed
     */
    public function subscribe($topic, SubscriberInterface $subscriber);
}