<?php

namespace app\modules\Patterns\EventChannel\controllers;

use app\modules\Patterns\EventChannel\models\Description;
use app\modules\Patterns\EventChannel\models\EventChannelJob;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\EventChannel\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $title = 'Канал событий';
        $discription = Description::getDescription();

        $eventChannel = new EventChannelJob();
        $item = $eventChannel->run(); // пример pattern interface

        return $this->render('index', compact('item' , 'title', 'discription'));
    }
}
