<?php

namespace app\modules\Patterns\Delegation\models;

class Description
{
    /**
     * @return string
     */
    public static function getDescription()
    {
        return "<pre>
        Делегирование (англ. Delegation) — основной шаблон проектирования, в котором объект внешне 
        выражает некоторое поведение, но в реальности передаёт ответственность за выполнение этого 
        поведения связанному объекту. 
        
        Шаблон делегирования является фундаментальной абстракцией, на основе которой реализованы 
        другие шаблоны - композиция (также называемая агрегацией), примеси (mixins) и аспекты (aspects).
        
        <a target='_blank' href='https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%B4%D0%B5%D0%BB%D0%B5%D0%B3%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F'>https://ru.wikipedia.org/wiki/Шаблон_делегирования</a>
        </pre>";
    }
}