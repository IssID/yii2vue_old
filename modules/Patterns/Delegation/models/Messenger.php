<?php

namespace app\modules\Patterns\Delegation\models;

use app\modules\Patterns\Delegation\interfaces\MessengerInterface;
use app\modules\Patterns\Delegation\models\Messengers\EmailMessenger;
use app\modules\Patterns\Delegation\models\Messengers\SmsMessenger;

/**
 * Class Messenger
 * @package app\modules\Patterns\Delegation\models
 */
class Messenger implements MessengerInterface
{
    /** @var MessengerInterface */
    private $messenger;

    /**
     * Messenger constructor.
     */
    public function __construct()
    {
        $this->toEmail();
    }

    /**
     * Отправление по электронной почте
     * @return $this
     */
    public function toEmail()
    {
        $this->messenger = new EmailMessenger();

        return $this;
    }

    /**
     * Отправление по СМС
     * @return $this
     */
    public function toSms()
    {
        $this->messenger = new SmsMessenger();

        return $this;
    }

    /**
     * Установить отправителя
     * @param $value
     * @return MessengerInterface
     */
    public function setSender($value): MessengerInterface
    {
        $this->messenger->setSender($value);

        return $this;
    }

    /**
     * Установить получателя
     * @param $value
     * @return MessengerInterface
     */
    public function setRecipient($value): MessengerInterface
    {
        $this->messenger->setRecipient($value);

        return $this;
    }

    /**
     * Установить сообщение
     * @param $value
     * @return MessengerInterface
     */
    public function setMessage($value): MessengerInterface
    {
        $this->messenger->setMessage($value);

        return $this;
    }

    /**
     * Отправить сообщение
     * @return bool
     */
    public function send(): bool
    {
        return $this->messenger->send();
    }
}