<?php

namespace app\modules\Patterns\Delegation\models\Messengers;

/**
 * Class SmsMessenger
 * @package app\modules\Patterns\Delegation\models\Messengers
 */
class SmsMessenger extends AbstractMessenger
{
    /**
     * @return bool
     */
    public function send(): bool
    {
        // this send logic
        return parent::send();
    }
}