<?php

namespace app\modules\Patterns\Delegation\models\Messengers;

use app\modules\Patterns\Delegation\interfaces\MessengerInterface;

/**
 * Class AbstractMessenger
 * @package app\modules\Patterns\Delegation\models\Messengers
 */
abstract class AbstractMessenger implements MessengerInterface
{
    /** @var string */
    protected $sender;
    /** @var string */
    protected $recipient;
    /** @var string */
    protected $message;

    /**
     * Установить отправителя
     * @param $value
     * @return MessengerInterface
     */
    public function setSender($value): MessengerInterface
    {
        $this->sender = $value;

        return $this;
    }

    /**
     * Установить получателя
     * @param $value
     * @return MessengerInterface
     */
    public function setRecipient($value): MessengerInterface
    {
        $this->recipient = $value;

        return $this;
    }

    /**
     * Установить сообщение
     * @param $value
     * @return MessengerInterface
     */
    public function setMessage($value): MessengerInterface
    {
        $this->message = $value;

        return $this;
    }

    /**
     * Отправить сообщение
     * @return bool
     */
    public function send(): bool
    {
        return true;
    }
}