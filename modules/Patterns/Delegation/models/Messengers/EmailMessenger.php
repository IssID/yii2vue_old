<?php

namespace app\modules\Patterns\Delegation\models\Messengers;

/**
 * Class EmailMessenger
 * @package app\modules\Patterns\Delegation\models\Messengers
 */
class EmailMessenger extends AbstractMessenger
{
    /**
     * @return bool
     */
    public function send(): bool
    {
        // this send logic
        return parent::send();
    }
}