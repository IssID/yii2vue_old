<?php

namespace app\modules\Patterns\Delegation;

/**
 * Class Module
 * @package app\modules\Patterns\Delegation
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\Delegation\controllers';
}
