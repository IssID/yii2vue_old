<?php

namespace app\modules\Patterns\Delegation\interfaces;

/**
 * Interface MessengerInterface
 * @package app\modules\Patterns\Delegation\interfaces
 */
interface MessengerInterface
{
    /**
     * Установить отправителя
     * @param $value
     * @return MessengerInterface
     */
    public function setSender($value): MessengerInterface;

    /**
     * Установить получателя
     * @param $value
     * @return MessengerInterface
     */
    public function setRecipient($value): MessengerInterface;

    /**
     * Установить сообщение
     * @param $value
     * @return MessengerInterface
     */
    public function setMessage($value): MessengerInterface;

    /**
     * Отправить сообщение
     * @return bool
     */
    public function send(): bool;
}