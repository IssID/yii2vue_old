<?php
/**
 * @var $item Messenger
 * @var $title string
 * @var $discription string
 * @var $model
 */

use app\modules\Patterns\Delegation\models\Messenger;
use yii\helpers\Html;
use yii\helpers\VarDumper;

$this->title = $title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<p><?= $discription ?></p>

<pre>
<?php VarDumper::dump($item); ?>
</pre>
<pre>
<?php VarDumper::dump($model); ?>
</pre>
