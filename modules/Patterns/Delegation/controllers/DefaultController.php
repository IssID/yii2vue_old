<?php

namespace app\modules\Patterns\Delegation\controllers;

use app\modules\Patterns\Delegation\models\Description;
use app\modules\Patterns\Delegation\models\Messenger;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\Delegation\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $title = 'Делегирование';
        $discription = Description::getDescription();

        // эти данные реализуются должны быть вынесены в форму (это всего лишь пример логики)
        $item = [];

        $model = new Messenger();

        $item['email'] = $model->setSender('sender@email.net')
            ->setRecipient('recipient@email.net')
            ->setMessage('Hello email messenger!')
            ->send();

        $item['sms'] = $model->toSms()
            ->setSender('+79008007060')
            ->setRecipient('+79008007061')
            ->setMessage('Hello SMS messenger!')
            ->send();

        return $this->render('index', compact('item', 'model', 'title', 'discription'));
    }
}
