<?php

namespace app\modules\Patterns\AbstractFactory\interfaces;

/**
 * Interface GuiFactoryInterface
 * @package app\modules\Patterns\AbstractFactory\interfaces
 */
interface GuiFactoryInterface
{
    /** @return ButtonInterface */
    public function buildButton(): ButtonInterface;

    /** @return CheckBoxInterface */
    public function buildCheckBox(): CheckBoxInterface;
}