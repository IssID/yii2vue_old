<?php

namespace app\modules\Patterns\AbstractFactory\interfaces;

/**
 * Interface CheckBoxInterface
 * @package app\modules\Patterns\AbstractFactory\interfaces
 */
interface CheckBoxInterface
{
    public function drow();
}