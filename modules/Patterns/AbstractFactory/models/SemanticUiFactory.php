<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\ButtonInterface;
use app\modules\Patterns\AbstractFactory\interfaces\CheckBoxInterface;
use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;

/**
 * Class SemanticUiFactory
 * @package app\modules\Patterns\AbstractFactory\models
 */
class SemanticUiFactory implements GuiFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonSemanticUi();
    }

    /**
     * @inheritDoc
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxSemanticUi();
    }
}