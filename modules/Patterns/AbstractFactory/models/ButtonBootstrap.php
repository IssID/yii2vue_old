<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\ButtonInterface;

/**
 * Class ButtonBootstrap
 * @package app\modules\Patterns\AbstractFactory\models
 */
class ButtonBootstrap implements ButtonInterface
{
    /**
     * @return string
     */
    public function drow()
    {
        return __METHOD__;
    }
}