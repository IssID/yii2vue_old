<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\CheckBoxInterface;

/**
 * Class CheckBoxBootstrap
 * @package app\modules\Patterns\AbstractFactory\models
 */
class CheckBoxBootstrap implements CheckBoxInterface
{
    /**
     * @return string
     */
    public function drow()
    {
        return __METHOD__;
    }
}