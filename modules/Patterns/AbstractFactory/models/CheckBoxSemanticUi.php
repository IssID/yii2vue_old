<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\CheckBoxInterface;

/**
 * Class CheckBoxSemanticUi
 * @package app\modules\Patterns\AbstractFactory\models
 */
class CheckBoxSemanticUi implements CheckBoxInterface
{
    /**
     * @return string
     */
    public function drow()
    {
        return __METHOD__;
    }
}