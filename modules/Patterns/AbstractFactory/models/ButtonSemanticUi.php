<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\ButtonInterface;

/**
 * Class ButtonSemanticUi
 * @package app\modules\Patterns\AbstractFactory\models
 */
class ButtonSemanticUi implements ButtonInterface
{
    /**
     * @return string
     */
    public function drow()
    {
        return __METHOD__;
    }
}