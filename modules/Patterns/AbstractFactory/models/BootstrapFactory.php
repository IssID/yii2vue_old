<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\ButtonInterface;
use app\modules\Patterns\AbstractFactory\interfaces\CheckBoxInterface;
use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;

/**
 * Class BootstrapFactory
 * @package app\modules\Patterns\AbstractFactory\models
 */
class BootstrapFactory implements GuiFactoryInterface
{

    /**
     * @return ButtonInterface
     */
    public function buildButton(): ButtonInterface
    {
        return new ButtonBootstrap();
    }

    /**
     * @inheritDoc
     */
    public function buildCheckBox(): CheckBoxInterface
    {
        return new CheckBoxBootstrap();
    }
}