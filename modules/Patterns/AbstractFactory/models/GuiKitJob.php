<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;

/**
 * Class GuiKitJob
 * @package app\modules\Patterns\AbstractFactory\models
 */
class GuiKitJob
{
    /**
     * @var GuiFactoryInterface
     */
    private $guiKit;

    /**
     * GuiKit constructor.
     * @throws \Exception
     */
    public function __construct(string $type = null)
    {
        if ($type) {
            $this->guiKit = (new GuiKitFactory())->getFactory($type);
        } else {
            $this->guiKit = (new GuiKitFactory())->getFactory('bootstrap');
        }
    }

    /**
     * @return array
     */
    public function run()
    {
        $result[] = $this->guiKit->buildButton()->drow();
        $result[] = $this->guiKit->buildCheckBox()->drow();

        return $result;
    }
}