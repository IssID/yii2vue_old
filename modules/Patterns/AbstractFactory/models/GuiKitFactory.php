<?php

namespace app\modules\Patterns\AbstractFactory\models;

use app\modules\Patterns\AbstractFactory\interfaces\GuiFactoryInterface;

/**
 * Class GuiKitFactory
 * @package app\modules\Patterns\AbstractFactory\models
 */
class GuiKitFactory
{
    /**
     * @param $type
     * @return GuiFactoryInterface
     * @throws \Exception
     */
    public function getFactory($type): GuiFactoryInterface
    {
        switch ($type) {
            case 'bootstrap':
                $factory = new BootstrapFactory();
                break;
            case 'semanticui':
                $factory = new SemanticUiFactory();
                break;
            default:
                throw new \Exception("Неизвестный тип фабрики [{$type}]");
        }

        return $factory;
    }
}