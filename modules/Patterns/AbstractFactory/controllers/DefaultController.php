<?php

namespace app\modules\Patterns\AbstractFactory\controllers;

use app\modules\Patterns\AbstractFactory\models\GuiKitJob;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\Patterns\AbstractFactory\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $item = [
            'title' => 'Абстрактная фабрика',
            'body' => 'AbstractFactory',
        ];

        $factory = new GuiKitJob(); // semanticui, bootstrap
        $item = $factory->run();

        return $this->render('index', compact('item'));
    }
}
