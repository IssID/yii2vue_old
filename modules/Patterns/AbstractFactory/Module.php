<?php

namespace app\modules\Patterns\AbstractFactory;

/**
 * Class Module
 * @package app\modules\Patterns\AbstractFactory
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\AbstractFactory\controllers';
}
