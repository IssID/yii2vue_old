<?php
namespace app\modules\Patterns\SimpleFactory\interfaces;


use app\modules\Patterns\Delegation\interfaces\MessengerInterface;

interface MessageSimpleFactoryInterface
{
    public function build(string $type): MessengerInterface;
}