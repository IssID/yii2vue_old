<?php

namespace app\modules\Patterns\SimpleFactory;

/**
 * Class Module
 * @package app\modules\Patterns\SimpleFactory
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\SimpleFactory\controllers';
}
