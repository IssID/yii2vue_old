<?php

namespace app\modules\Patterns\SimpleFactory\controllers;

use app\modules\Patterns\SimpleFactory\models\SimpleFactory;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\SimpleFactory\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $item = [
            'title' => 'Простая фабрика',
            'body' => 'SimpleFactory'
        ];

        $factory = new SimpleFactory();
        $appMailMessanger = $factory->build('email');
        $appSmsMessanger = $factory->build('sms');

        return $this->render('index', compact('item', 'appMailMessanger', 'appSmsMessanger'));
    }
}
