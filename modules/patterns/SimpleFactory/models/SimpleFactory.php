<?php
namespace app\modules\Patterns\SimpleFactory\models;


use app\modules\Patterns\Delegation\interfaces\MessengerInterface;
use app\modules\Patterns\Delegation\models\Messenger;
use app\modules\Patterns\SimpleFactory\interfaces\MessageSimpleFactoryInterface;
use yii\base\Exception;

class SimpleFactory implements MessageSimpleFactoryInterface
{

    /**
     * @param string $type
     * @return MessengerInterface
     * @throws Exception
     */
    public function build(string $type = 'email'): MessengerInterface
    {
        $messanger = new Messenger();

        switch ($type) {
            case 'email':
                $messanger->toEmail();
                $messanger->setSender('admin@site.test');
                $messanger->setMessage("default email message");
                break;
            case 'sms':
                $messanger->toSms();
                $messanger->setSender('88002000123');
                $messanger->setMessage("default message sms");
                break;
            default:
                throw new Exception("Неизвестный тип [{$type}]");
        }

        return $messanger;
    }
}