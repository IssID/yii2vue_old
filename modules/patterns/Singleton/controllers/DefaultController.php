<?php

namespace app\modules\Patterns\Singleton\controllers;

use app\modules\Patterns\Singleton\models\AdvancedSingleton;
use app\modules\Patterns\Singleton\models\SimpleSingleton;
use app\modules\Patterns\Singleton\models\SingletonTrait;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\Singleton\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $item = [
            'title' => 'Одиночка',
            'body' => 'Singleton'
        ];

        $result['SimpleSingleton1'] = SimpleSingleton::getInstance();
        $result['SimpleSingleton1']->setValue('SimpleSingleton');
        $result['SimpleSingleton2'] = SimpleSingleton::getInstance();
        $result['SimpleSingletonResult'] = $result['SimpleSingleton1'] === $result['SimpleSingleton2'];

        $result['AdvancedSingleton1'] = AdvancedSingleton::getInstance();
        $result['AdvancedSingleton1']->setValue('AdvancedSingleton');
        $result['AdvancedSingleton2'] = AdvancedSingleton::getInstance();
        $result['AdvancedSingletonResult'] = $result['AdvancedSingleton1'] === $result['AdvancedSingleton2'];

        return $this->render('index', compact('item', 'result'));
    }
}
