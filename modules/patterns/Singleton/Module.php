<?php

namespace app\modules\Patterns\Singleton;

/**
 * Class Module
 * @package app\modules\Patterns\Singleton
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\Singleton\controllers';
}
