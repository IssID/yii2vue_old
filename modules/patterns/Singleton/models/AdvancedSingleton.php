<?php
namespace app\modules\Patterns\Singleton\models;
/**
 * Class AdvancedSingleton
 *
 * @package app\modules\Patterns\Singleton\models
 */
class AdvancedSingleton
{
    use SingletonTrait;

    private $value;

    public function setValue($val) {
        $this->value = $val;
    }
}