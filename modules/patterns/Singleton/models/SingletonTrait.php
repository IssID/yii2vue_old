<?php

namespace app\modules\Patterns\Singleton\models;


trait SingletonTrait
{
    /**
     * @var SingletonTrait|null
     */
    private static $instance = null;

    /**
     * Запрещаем прямое создание
     *
     * SingletonTrait constructor.
     */
    private function __construct()
    {
        //
    }

    /**
     * Запрещаем клонирование
     */
    private function __clone()
    {
        //
    }

    /**
     * Запрещаем десериализацию
     */
    private function __wakeup()
    {
        //
    }

    /**
     * @return SingletonTrait|null
     */
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new static());
    }

}