<?php
namespace app\modules\Patterns\Singleton\models;

class SimpleSingleton
{
    /**
     * @var SimpleSingleton
     */
    private static $instance;

    /**
     * @var string
     */
    private $value;

    /**
     * @return SimpleSingleton|static
     */
    public static function getInstance(){
        return static::$instance ?? (static::$instance = new static());
    }

    /**
     * @param $val
     */
    public function setValue($val) {
        $this->value = $val;
    }
}