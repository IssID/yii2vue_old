<?php
namespace app\modules\Patterns\StaticFactory\interfaces;


use app\modules\Patterns\Delegation\interfaces\MessengerInterface;

interface MessageStaticFactoryInterface
{
    public static function build(string $type): MessengerInterface;
}