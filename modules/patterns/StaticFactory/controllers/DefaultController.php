<?php

namespace app\modules\Patterns\StaticFactory\controllers;

use app\modules\Patterns\StaticFactory\models\StaticFactory;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\Patterns\StaticFactory\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $item = [
            'title' => 'Статическая фабрика',
            'body' => 'StaticFactory'
        ];

        $appMailMessanger = StaticFactory::build('email');
        $appSmsMessanger = StaticFactory::build('sms');

        return $this->render('index', compact('item', 'appMailMessanger', 'appSmsMessanger'));
    }
}
