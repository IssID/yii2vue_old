<?php

namespace app\modules\Patterns\StaticFactory;

/**
 * Class Module
 * @package app\modules\Patterns\StaticFactory
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\Patterns\StaticFactory\controllers';
}
