<?php
namespace app\modules\Patterns\StaticFactory\models;


use app\modules\Patterns\Delegation\interfaces\MessengerInterface;
use app\modules\Patterns\Delegation\models\Messenger;
use app\modules\Patterns\StaticFactory\interfaces\MessageStaticFactoryInterface;
use yii\base\Exception;

class StaticFactory implements MessageStaticFactoryInterface
{

    /**
     * @param string $type
     * @return MessengerInterface
     * @throws Exception
     */
    public static function build(string $type = 'email'): MessengerInterface
    {
        $messanger = new Messenger();

        switch ($type) {
            case 'email':
                $messanger->toEmail();
                $sender = 'admin@site.test';
                break;
            case 'sms':
                $messanger->toSms();
                $sender = '88002000123';
                break;
            default:
                throw new Exception("Неизвестный тип [{$type}]");
        }
        $messanger
            ->setSender($sender)
            ->setMessage("default message");

        return $messanger;
    }
}